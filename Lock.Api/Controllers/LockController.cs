﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Lock.Domain.Ports.Managers;
using Microsoft.AspNetCore.Mvc;

namespace Lock.Api.Controllers
{
    /// <summary>
    /// Controller which provides Locks REST functionality.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/locks")]
    [ApiController]
    public class LockController : ControllerBase
    {
        private readonly ILockManager _lockManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="LockController"/> class.
        /// </summary>
        /// <param name="lockManager">The lock manager.</param>
        public LockController(ILockManager lockManager)
        {
            _lockManager = lockManager ?? throw new ArgumentNullException(nameof(lockManager));
        }

        /// <summary>
        /// Opens the given lock asynchronous.
        /// </summary>
        /// <param name="id">The lock identifier.</param>
        /// <param name="cancellationToken">Cancellation token injected by the hosting environment.</param>
        [HttpPost("{id}")]
        [ProducesResponseType(204)]
        public async Task<ActionResult> OpenAsync(string id, CancellationToken cancellationToken)
        {
            // TODO: add authentication token and extract user from token
            var userId = "testUserId";

            await _lockManager.OpenAsync(id, userId, cancellationToken);

            return NoContent();
        }
    }
}