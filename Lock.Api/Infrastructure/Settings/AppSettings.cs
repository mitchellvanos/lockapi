﻿namespace Lock.Api.Infrastructure.Settings
{
    /// <summary>
    /// Application settings.
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Application name.
        /// </summary>
        public string ApplicationName { get; set; }
    }
}
