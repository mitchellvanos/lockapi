﻿namespace Lock.Api.Infrastructure.Settings
{
    /// <summary>
    /// MongoDb connectivity settings.
    /// </summary>
    public class MongoDbSettings
    {
        /// <summary>
        /// Database connection string. 
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Database name.
        /// </summary>
        public string DatabaseName { get; set; }
    }
}
