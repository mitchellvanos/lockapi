﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Lock.Api.Infrastructure.Swagger
{
    /// <summary>
    /// The RemoveVersionFromParameter class.
    /// </summary>
    /// <seealso cref="Swashbuckle.AspNetCore.SwaggerGen.IOperationFilter" />
    [ExcludeFromCodeCoverage]
    public class RemoveVersionFromParameter : IOperationFilter
    {
        /// <summary>
        /// Applies the specified operation.
        /// </summary>
        /// <param name="operation">The operation.</param>
        /// <param name="context">The context.</param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var versionParameter = operation.Parameters.FirstOrDefault(x => x.Name == "api-version");
            operation.Parameters.Remove(versionParameter);
        }
    }
}
