﻿using System;
using System.IO;
using System.Reflection;
using FluentValidation.AspNetCore;
using Lock.Api.Infrastructure.Settings;
using Lock.Api.Infrastructure.Swagger;
using Lock.Domain;
using Lock.Domain.Managers;
using Lock.Domain.Models;
using Lock.Domain.Ports.Handlers;
using Lock.Domain.Ports.Managers;
using Lock.Domain.Ports.Repositories;
using Lock.MongoDb;
using Lock.LockHandler;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Swagger;

namespace Lock.Api
{
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public IConfiguration Configuration { get; }


        /// <summary>
        /// Configures the services.
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940        
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Configure MongoDb
            var mongoDbSettings = Configuration.GetSection("MongoDbSettings").Get<MongoDbSettings>();
            var connectionString = $"{mongoDbSettings.ConnectionString}/{mongoDbSettings.DatabaseName}";

            var client = new MongoClient(connectionString);
            var database = client.GetDatabase(mongoDbSettings.DatabaseName);
            
            services.AddSingleton(collection => database.GetCollection<LockEventHistory>("LockEventHistories"));

            // Configure Mvc and configure fluent validations
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddFluentValidation(
                options =>
                {
                    options.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    options.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                    // Do not call SetValidator manually.
                    options.ImplicitlyValidateChildProperties = true;
                });

            // Configure DI
            services.AddTransient<ISomeLockHandler, SomeLockHandler>();
            services.AddTransient<ILockEventHistoryRepository, LockEventHistoryRepository>();
            services.AddTransient<ILockManager, LockManager>();
            services.AddTransient<ITimeProvider, TimeProvider>();

            // In order to have clean poco's we demand that id's are string and called Id.
            // Then we use a Mongo convention that can be used as BsonId.
            // We also use a convention to show the string values for Enums in the DB to keep the data more readable
            var conventionPack = new ConventionPack { new StringObjectIdConvention(), new EnumRepresentationConvention(BsonType.String) };
            ConventionRegistry.Register("LockConventions", conventionPack, _ => true);

            // Configure swagger
            services.AddSwaggerGen(
                options =>
                {
                    options.SwaggerDoc("v1", new Info { Title = "Lock API", Version = "v1" });

                    // This call remove version from parameter, without it we will have version as parameter 
                    // for all endpoints in swagger UI
                    options.OperationFilter<RemoveVersionFromParameter>();

                    // integrate xml comments                    
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    options.IncludeXmlComments(xmlPath);
                });
        }

        /// <summary>
        /// Configures the specified application. This method gets called by the runtime. Use this method to configure the HTTP request pipeline.        
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Lock API V1");
            });

            app.UseMvc();
        }
    }
}
