﻿using System;

namespace Lock.Domain
{
    public interface ITimeProvider
    {
        DateTime UtcNow { get; }
    }
}
