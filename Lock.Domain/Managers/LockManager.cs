﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Lock.Domain.Models;
using Lock.Domain.Ports.Handlers;
using Lock.Domain.Ports.Managers;
using Lock.Domain.Ports.Repositories;
using Microsoft.Extensions.Logging;

namespace Lock.Domain.Managers
{
    public class LockManager : ILockManager
    {
        private readonly ISomeLockHandler _lockHandler;
        private readonly ILockEventHistoryRepository _lockEventHistoryRepository;
        private readonly ILogger _logger;
        private readonly ITimeProvider _timeProvider;

        /// <inheritdoc />
        public LockManager(ISomeLockHandler lockHandler, ILockEventHistoryRepository lockEventHistoryRepository, ILogger<LockManager> logger, ITimeProvider timeProvider)
        {
            
            _lockHandler = lockHandler ?? throw new ArgumentNullException(nameof(lockHandler));
            _lockEventHistoryRepository = lockEventHistoryRepository ?? throw new ArgumentNullException(nameof(lockEventHistoryRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _timeProvider = timeProvider ?? throw new ArgumentNullException(nameof(timeProvider));
        }

        /// <inheritdoc />
        public async Task OpenAsync(string id, string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Open the lock
            _logger.LogDebug(
                "Trying to open lock {lockId} for user {userId}.",
                id, userId);

            // TODO: Make method async once it's implemented
            _lockHandler.OpenAsync(id, cancellationToken);

            // TODO: Add something of a message bus or a saga and publish the lock event to ensure eventual consistency in case of downtime,
            // TODO: at the very least we need to make sure the event does not get lost
            // Store the lock opened event
            var lockEvent = new LockEvent
                        {
                            Id = Guid.NewGuid().ToString(),
                            UserId = userId,
                            CreatedOn = _timeProvider.UtcNow,
                            Type = LockEventType.Open
                        };

            _logger.LogDebug(
                "Trying to add newly created lock event {LockEventId} for lock {lockId} and user {userId}.",
                lockEvent.Id, id, userId);
            await _lockEventHistoryRepository.UpsertLockEventAsync(id, lockEvent, cancellationToken);
        }
    }
}
