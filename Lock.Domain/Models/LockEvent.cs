﻿using System;

namespace Lock.Domain.Models
{
    public class LockEvent
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedOn { get; set; }
        public LockEventType Type { get; set; }
    }

    public enum LockEventType
    {
        Open,
        Close
    }
}
