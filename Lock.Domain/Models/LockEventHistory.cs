﻿using System.Collections.Generic;

namespace Lock.Domain.Models
{
    public class LockEventHistory
    {
        public string Id { get; set; }
        public List<LockEvent> Events { get; set; }
    }
}
