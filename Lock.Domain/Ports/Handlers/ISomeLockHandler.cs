﻿using System.Threading;

namespace Lock.Domain.Ports.Handlers
{
    public interface ISomeLockHandler
    {
        /// <summary>
        /// Opens the given lock asynchronous.
        /// </summary>
        /// <param name="id">The lock identifier.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        void OpenAsync(string id, CancellationToken cancellationToken = default(CancellationToken));
    }
}
