﻿using System.Threading;
using System.Threading.Tasks;

namespace Lock.Domain.Ports.Managers
{
    public interface ILockManager
    {
        /// <summary>
        /// Opens the given lock asynchronous.
        /// </summary>
        /// <param name="id">The lock identifier.</param>
        /// <param name="user">The user identifier.</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task OpenAsync(string id, string user, CancellationToken cancellationToken = default(CancellationToken));
    }
}
