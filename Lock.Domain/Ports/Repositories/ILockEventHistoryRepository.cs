﻿using System.Threading;
using System.Threading.Tasks;
using Lock.Domain.Models;

namespace Lock.Domain.Ports.Repositories
{
    /// <summary>
    /// Lock event history repository interface class.
    /// </summary>
    public interface ILockEventHistoryRepository
    {
        Task UpsertLockEventAsync(string lockId, LockEvent lockEvent, CancellationToken cancellationToken = default(CancellationToken));
    }
}
