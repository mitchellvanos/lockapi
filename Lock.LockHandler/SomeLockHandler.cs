﻿using System.Threading;
using Lock.Domain.Ports.Handlers;

namespace Lock.LockHandler
{
    /// <inheritdoc />
    public class SomeLockHandler : ISomeLockHandler
    {
        /// <inheritdoc />
        public void OpenAsync(string id, CancellationToken cancellationToken = default(CancellationToken))
        {
            // TODO: open sesame, and make the method async
        }
    }
}
