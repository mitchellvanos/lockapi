﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Lock.Domain.Models;
using Lock.Domain.Ports.Repositories;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Lock.MongoDb
{
    /// <inheritdoc />
    public class LockEventHistoryRepository : ILockEventHistoryRepository
    {
        private readonly IMongoCollection<LockEventHistory> _lockEventHistories;
        private readonly ILogger _logger;

        /// <inheritdoc />
        public LockEventHistoryRepository(IMongoCollection<LockEventHistory> lockEventHistories, ILogger<LockEventHistoryRepository> logger)
        {
            _lockEventHistories = lockEventHistories ?? throw new ArgumentNullException(nameof(lockEventHistories));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <inheritdoc />
        public async Task UpsertLockEventAsync(string lockId, LockEvent lockEvent, CancellationToken cancellationToken = default(CancellationToken))
        {
            var updateBuilder = Builders<LockEventHistory>.Update;
            await _lockEventHistories.FindOneAndUpdateAsync<LockEventHistory>(
                x => x.Id == lockId && !x.Events.Any(y => y.Id == lockEvent.Id),
                updateBuilder.AddToSet(x => x.Events, lockEvent),
                new FindOneAndUpdateOptions<LockEventHistory> { IsUpsert = true },
                cancellationToken);

            _logger.LogDebug("Successfully added lock event {LockEventId} for lock {lockId} and user {userId} to the database.", lockEvent.Id, lockId, lockEvent.UserId);
        }
    }
}
