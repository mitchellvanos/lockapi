﻿using System.Diagnostics.CodeAnalysis;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Lock.MongoDb
{
    [ExcludeFromCodeCoverage]
    public class StringObjectIdConvention : ConventionBase, IPostProcessingConvention
    {
        public void PostProcess(BsonClassMap classMap)
        {
            var idMap = classMap.IdMemberMap;

            if (idMap != null && idMap.MemberName == "Id" && idMap.MemberType == typeof(string))
                idMap.SetIdGenerator(StringObjectIdGenerator.Instance);
        }
    }
}
