﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Lock.Api.Controllers;
using Lock.Domain.Ports.Managers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Lock.UnitTests.Controllers
{
    public class LockControllerTests
    {
        [Theory, AutoData]
        public async Task OpenLockAsync_Success(string lockId)
        {
            // Arrange
            // TODO: don't hard-code the user id, instead mock the user identity once it's implemented
            var userId = "testUserId"; 

            var lockManager = new Mock<ILockManager>();
            lockManager.Setup(x => x.OpenAsync(lockId, userId, It.IsAny<CancellationToken>())).Returns(Task.CompletedTask).Verifiable();

            var sut = new LockController(lockManager.Object);

            // Act
            var response = await sut.OpenAsync(lockId, It.IsAny<CancellationToken>());
            var result = response as NoContentResult;

            // Assert
            result.StatusCode.Should().Be((int)HttpStatusCode.NoContent);
            lockManager.Verify();
        }
    }
}
