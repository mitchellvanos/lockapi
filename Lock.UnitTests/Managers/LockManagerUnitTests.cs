﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Lock.Domain;
using Lock.Domain.Managers;
using Lock.Domain.Models;
using Lock.Domain.Ports.Handlers;
using Lock.Domain.Ports.Repositories;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Lock.UnitTests.Managers
{
    public class LockManagerUnitTests
    {
        public class LocksControllerTests
        {
            [Theory, AutoData]
            public async Task PostLockAsync_Success(string lockId, string userId, DateTime now)
            {
                // Arrange
                var someLockHandler = new Mock<ISomeLockHandler>();
                someLockHandler.Setup(x => x.OpenAsync(lockId, It.IsAny<CancellationToken>())).Verifiable();
                
                var lockEventHistoryRepository = new Mock<ILockEventHistoryRepository>();
                lockEventHistoryRepository.Setup(x => x.UpsertLockEventAsync(lockId, It.Is<LockEvent>(y => y.UserId == userId && y.CreatedOn == now), It.IsAny<CancellationToken>())).Returns(Task.CompletedTask).Verifiable();

                var loggerMock = new Mock<ILogger<LockManager>>();

                var timeProviderMock = new Mock<ITimeProvider>();
                timeProviderMock.Setup(x => x.UtcNow).Returns(now);

                var sut = new LockManager(someLockHandler.Object, lockEventHistoryRepository.Object, loggerMock.Object, timeProviderMock.Object);

                // Act
                await sut.OpenAsync(lockId, userId, It.IsAny<CancellationToken>());

                // Assert
                lockEventHistoryRepository.Verify();
                someLockHandler.Verify();
                timeProviderMock.Verify();
            }
        }
    }
}
